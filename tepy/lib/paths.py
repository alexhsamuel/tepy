from   pathlib import Path

#-------------------------------------------------------------------------------

def ensure(path):
    return path if isinstance(path, Path) else Path(path)



