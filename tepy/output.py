import contextlib
import os
import sys
import tempfile

#-------------------------------------------------------------------------------

@contextlib.contextmanager
def redirect12(file):
    if sys.stdout.fileno() == 1:
        sys.stdout.flush()

    tmp_fd = file.fileno()
    old1 = os.dup(1)
    os.dup2(tmp_fd, 1)
    old2 = os.dup(2)
    os.dup2(tmp_fd, 2)

    try:
        yield

    finally:
        if sys.stdout.fileno() == 1:
            sys.stdout.flush()

        os.dup2(old1, 1)
        os.dup2(old2, 2)


@contextlib.contextmanager
def capture():
    output = None

    def get():
        nonlocal output
        return output

    with tempfile.NamedTemporaryFile() as out_file:
        with redirect12(out_file):
            yield get

        out_file.seek(0)
        output = out_file.read()


