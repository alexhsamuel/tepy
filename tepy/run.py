import asyncio
import collections
import concurrent.futures
import enum
import inspect
import logging
import traceback
import types
import sys

from   . import output

log = logging.getLogger(__name__)

#-------------------------------------------------------------------------------

# FIXME: Replace with an "enum" that doesn't actually enumerate.
class Enum(enum.Enum):

    @classmethod
    def ensure(cls, obj):
        """
        Converts `obj` to a member.

        :param obj:
          Either a member or the name of one.
        """
        if isinstance(obj, cls):
            return obj
        try:
            return cls[str(obj)]
        except KeyError:
            pass
        raise ValueError(f"not in {cls}: {obj}")



#-------------------------------------------------------------------------------

State = Enum("State", ["PASS", "FAIL"])

class TestResult:

    def __init__(self, state):
        self.state = State.ensure(state)
        self.output = None


    def __str__(self):
        return self.state.name



class TestPass(TestResult):

    def __init__(self):
        super().__init__(State.PASS)



class TestException(TestResult):

    def __init__(self, exc, tb):
        super().__init__(State.FAIL)
        self.exc = exc
        self.tb = tb



def run_callable(fn):
    try:
        with output.capture() as get_output:
            fn()
    except KeyboardInterrupt:
        # Let it through.
        raise
    except BaseException:
        _, exc, tb = sys.exc_info()
        # Omit the bottom of the stack up to this frame.
        while tb.tb_frame.f_code.co_name != "run_callable":
            tb = tb.tb_next
        # Omit this frame too.  Above here is target code.
        tb = tb.tb_next
        # Render the traceback.  It's not pickleable.
        tb = "".join(traceback.format_exception(None, exc, tb))
        res = TestException(exc, tb)
    else:
        res = TestPass()

    res.output = get_output()
    return res



#-------------------------------------------------------------------------------

def run1(ctx, spec):
    log.debug(f"loading: {spec}")
    test = spec.load()
    log.debug(f"running: {spec}")
    res = test(ctx)
    log.debug(f"result: {spec} → {res}")
    return spec, res


def run_immediate(specs):
    ctx = None
    for spec in specs:
        yield run1(ctx, spec)


def run_in_executor(executor, specs):
    ctx = None
    def submit(spec):
        fut = executor.submit(run1, ctx, spec)
        fut.spec = spec
        return fut

    futs = [ submit(s) for s in specs ]
    return ( f.result() for f in concurrent.futures.as_completed(futs) )


def run_in_processes(specs):
    with concurrent.futures.ProcessPoolExecutor(max_workers=8) as executor:
        yield from run_in_executor(executor, specs)


#-------------------------------------------------------------------------------

def get_stats(results):
    counts = {}

    def f():
        for spec, res in results:
            counts[res.state] = counts.get(res.state, 0) + 1
            yield spec, res

    return f(), counts


