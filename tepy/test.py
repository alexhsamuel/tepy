import asyncio
from   typing import Callable

from   .run import run_callable

#-------------------------------------------------------------------------------

class FnTest:
    """
    A test implemented as a callable.
    """

    fn: Callable

    def __init__(self, fn):
        self.fn = fn


    def __call__(self, ctx):
        return run_callable(self.fn)



class AsyncFnTest:
    """
    A test implemented as an async callable.
    """

    fn: Callable

    def __init__(self, fn):
        self.fn = fn


    def __call__(self, ctx):
        return run_callable(lambda: asyncio.run(self.fn()))



