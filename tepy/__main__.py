import argparse
import concurrent.futures
import logging
import sys

from   . import collect, run
from   .lib.terminal import get_width

#-------------------------------------------------------------------------------

STATE_MARKERS = {
    run.State.PASS: "\x1b[32mPASS\x1b[m",
    run.State.FAIL: "\x1b[31mFAIL\x1b[m",
}

def collect_paths(paths):
    return ( s for p in paths for s in collect.collect_source_file(p) )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--collect", action="store_true", default=False,
        help="only collect and list tests")
    parser.add_argument(
        "--log-level", metavar="LEVEL", default="WARNING",
        help="log at LEVEL")
    parser.add_argument(
        "paths", metavar="PATH", nargs="*",
        help="load tests from PATH")
    args = parser.parse_args()

    logging.basicConfig(
        level=args.log_level.upper(),
        format="%(levelname)s %(name)s | %(msg)s",
    )

    with concurrent.futures.ProcessPoolExecutor(max_workers=8) as executor:
        specs = collect_paths(args.paths)

        if args.collect:
            for spec in collect_paths(args.paths):
                print(spec)
            return

        results = run.run_in_processes(specs)
        results, stats = run.get_stats(results)

        width = get_width()
        for spec, res in results:
            if res.state == run.State.PASS:
                continue
            print("=" * width)
            print(f"{spec}: {STATE_MARKERS[res.state]}")
            if res.output not in (None, ""):
                print("-" * width)
                sys.stdout.buffer.write(res.output)
            if hasattr(res, "tb"):
                print("-" * width)
                print(res.tb, end="")

    print(stats)


if __name__ == "__main__":
    main()

