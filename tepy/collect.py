import inspect
import logging
from   pathlib import Path
from   typing import Iterable

from   .lib import py

log = logging.getLogger(__name__)

#-------------------------------------------------------------------------------

class SourceObjSpec:
    """
    A test from a named object in a Python source file.
    """

    path: Path
    qualname: str

    def __init__(self, path: Path, qualname: str):
        assert all( p.isidentifier() for p in qualname.split(".") )

        self.path = path
        self.qualname = qualname


    def __str__(self):
        return f"{self.path}:{self.qualname}"


    def load(self):
        from .test import FnTest, AsyncFnTest

        fn = py.look_up(py.load_source_file(self.path), self.qualname)
        return (
            AsyncFnTest if inspect.iscoroutinefunction(fn)
            else FnTest
        )(fn)



#-------------------------------------------------------------------------------

def collect_source_file(path) -> Iterable:
    """
    Collects test specs from a test source file.
    """
    log.debug(f"collecting source file: {path}")
    module = py.load_source_file(path)

    for name in dir(module):
        if name.startswith("test"):
            obj = getattr(module, name)
            if callable(obj):
                yield SourceObjSpec(path, name)


