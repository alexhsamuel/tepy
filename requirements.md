# Test states

- pass
- fail
- skip(reason)


# Test types

- Python function ala `pytest`.
- async Python function ala `@pytest.mark.asyncio`
- Python class ala `unittest`.

Ways to run a Python invocation test:
- in the same old Python interpreter
  - synchronously
  - async
  - in a thread / process executor
  - in a one-off subprocess
  - in a subprocess worker
- in another interpreter in the same process
- via some kind of test worker (procstar agent?)

When running a Python test, control:
- cwd (tmpdir)
- env
- umask


# Test outputs

- state
- captured stdout
- captured stderr
- or, combined captured stdout/stderr
- captured logging
- output value / exit status
- stack trace
- residual files (if run in tmpdir)
- timing
- metadata


# Features

- control test isolation, i.e. run in subprocesses
- control test order, e.g. stable order, or shuffle
- collect tests
  - store collected tests
  - cache collected tests
  - fs watcher for test add/del/chg
  - stream collected tests, so they can start running immediately
- collect results
  - store results
  - load, query, show results
  - browse results
  - rerun based on previous results

