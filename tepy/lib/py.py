import functools
from   pathlib import Path
import types

#-------------------------------------------------------------------------------

@functools.cache
def load_source_file(path) -> types.ModuleType:
    """
    Loads a Python source file into a new module object, similar to `import`.
    """
    path = Path(path)

    with open(path) as file:
        source = file.read()
    code = compile(source, path, "exec")
    module = types.ModuleType(path.with_suffix("").name)
    module.__file__ = str(path)
    exec(code, module.__dict__)
    return module


def look_up(obj, qualname):
    """
    Performs recursive lookup of dotted `qualname` in `obj`.
    """
    parts = qualname.split(".")
    assert all( p.isidentifier() for p in parts )

    for part in parts:
        obj = getattr(obj, part)
    return obj


