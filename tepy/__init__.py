import functools
from   pathlib import Path
import tempfile

#-------------------------------------------------------------------------------

def tmpdir():
    def wrapper(fn):
        @functools.wraps(fn)
        def wrapped(*args, **kw_args):
            with tempfile.TemporaryDirectory() as tmpdir:
                return fn(Path(tmpdir), *args, **kw_args)
        return wrapped
    return wrapper


